/* global _ */

;(function (window, angular) {
  'use strict'

  var angularSaneDropdown = angular.module('angularSaneDropdown', [])

  angularSaneDropdown
  .service('SaneHydrationService', ['$q',
    function ($q) {
      var service = {
        findMatchingOption: function (hydrationKey, keyLocation, list) {
          // This method finds the object in the list that matches the hydrationKey and returns it
          var deferred = $q.defer()

          var matchingOption = _.find(list, function (item) {
            return item[keyLocation] === hydrationKey
          })

          if (!_.isEmpty(matchingOption) && !_.isUndefined(matchingOption)) {
            // If it's not empty or undefined
            // return the full matching object
            deferred.resolve(matchingOption)
          } else {
            // Otherwise, return null so we know there wasn't a match
            deferred.resolve(null)
          }

          return deferred.promise
        },

        rehydrateData: function (originalObject, hydrationKey, keyLocation, list) {
          // `hydrationKey` is the actual data (e.g. '551dec81bf9df6c0c038a431' as a Mongo ObjectId string)
          // off the `originalObject` you want to find
          // at the `keyLocation` (e.g. '_id')
          // on each item out of the `list`
          //
          // This method will set the matching option found with findMatchingOption to the `_selectedOption` key of whatever `originalObject` is passed in
          originalObject._selectedOption = service.findMatchingOption(hydrationKey, keyLocation, list)
        },

        rehydrateDataForArray: function (originalArray, hydrationKey, keyLocation, list) {
          // This method will run rehydrateData for each element in originalArray
          _.each(originalArray, function (item) {
            service.rehydrateData(item, hydrationKey, keyLocation, list)
          })
        }
      }

      return service
    }])
  .directive('saneDropdown', ['$log', 'SaneHydrationService', function ($log, SaneHydrationService) {
    return {
      restrict: 'E',
      template: '<select ng-model="__model__.__tracker__" ng-options="option.{{dropdownDisplayName}} for option in dropdownOptions track by option[dropdownTrackBy]" ng-disabled="dropdownDisabled"> name="{{dropdownFormName}}" ng-required="dropdownRequired" <option value="" ng-show="dropdownEmptyOption">{{dropdownEmptyOption}}</option> </select>',
      scope: {
        // Required. This should only be a string to work properly
        dropdownModel: '=',

        // Required. Should always be an array
        dropdownOptions: '=',

        // Optional. Must be a string naming the key you want to track, e.g. '_id'
        dropdownTrackBy: '=',

        // Optional. Will disable the dropdown
        dropdownDisabled: '=',

        // Optional. Must be a string. Will be the empty state placeholder option if it is set.
        dropdownEmptyOption: '=',

        // Optional. Must be a string naming the key you want to be the display name for each option. e.g. 'name'
        dropdownDisplayName: '=',

        // Optional. Will be the name of this select element for use in forms
        dropdownFormName: '=',

        // Optional. Will set this select element as required for a form
        dropdownRequired: '=',

        // Optional. Must be a function name that will be called back when the dropdown selection changes. E.g. 'dropdownChangedCallback'
        dropdownChanged: '=',

        // Optional.
        // Feeds out the full object that is chosen in the dropdown from the dropdownOptions array
        // This will be changed every time the selection is changed in the dropdown it is the easiest way to get a full copy of the selection outside the directive
        dropdownSelectedOption: '='
      },
      link: function ($scope, element, attributes) {
        // Example of using this directive:
        // <sane-dropdown
        //   dropdown-model="originalObject._id"
        //   dropdown-options="dropdownOptions"
        //   dropdown-selected-option="dropdownSelectedOption"
        //   dropdown-track-by="'_id'"
        //   dropdown-display-name="'name'"
        //   dropdown-form-name="'test'"
        //   dropdown-required="false"
        //   dropdown-disabled="false"
        //   dropdown-empty-option="'– Select Something –'"
        //   dropdown-changed="dropdownChanged">
        // </sane-dropdown>

        // // This initializeOptions function is not being utilized yet due to some issues it was causing
        // // but it should be re-enabled once those problems are resolved
        // function initializeOptions() {
        //   if (_.isUndefined($scope.dropdownModel) ||
        //     _.isEmpty($scope.dropdownModel) ||
        //     _.isNull($scope.dropdownModel)) {
        //     // ============= Required =============
        //     $log.error('dropdown-model is required for sane-dropdown!')
        //   }
        //   if (_.isUndefined($scope.dropdownOptions) ||
        //     _.isEmpty($scope.dropdownOptions) ||
        //     _.isNull($scope.dropdownOptions)) {
        //     // ============= Required =============
        //     $log.error('dropdown-options is required for sane-dropdown!')
        //   }
        //   if (_.isUndefined($scope.dropdownTrackBy) ||
        //     _.isEmpty($scope.dropdownTrackBy) ||
        //     _.isNull($scope.dropdownTrackBy)) {
        //     // ============= Optional =============
        //     // Defaults to '_id'
        //     $scope.dropdownTrackBy = '_id'
        //   }
        //   if (_.isUndefined($scope.dropdownDisabled) ||
        //     _.isEmpty($scope.dropdownDisabled) ||
        //     _.isNull($scope.dropdownDisabled)) {
        //     // ============= Optional =============
        //     // Defaults to false
        //     $scope.dropdownDisabled = false
        //   }
        //   if (_.isUndefined($scope.dropdownEmptyOption) ||
        //     _.isEmpty($scope.dropdownEmptyOption) ||
        //     _.isNull($scope.dropdownEmptyOption)) {
        //     // ============= Optional =============
        //     // Defaults to undefined
        //   }
        //   if (_.isUndefined($scope.dropdownDisplayName) ||
        //     _.isEmpty($scope.dropdownDisplayName) ||
        //     _.isNull($scope.dropdownDisplayName)) {
        //     // ============= Optional =============
        //     // Defaults to 'name'
        //     $scope.dropdownDisplayName = 'name'
        //   }
        //   if (_.isUndefined($scope.dropdownFormName) ||
        //     _.isEmpty($scope.dropdownFormName) ||
        //     _.isNull($scope.dropdownFormName)) {
        //     // ============= Optional =============
        //     // Defaults to undefined
        //   }
        //   if (_.isUndefined($scope.dropdownRequired) ||
        //     _.isEmpty($scope.dropdownRequired) ||
        //     _.isNull($scope.dropdownRequired)) {
        //     // ============= Optional =============
        //     // Defaults to false
        //     $scope.dropdownRequired = false
        //   }
        //   if (_.isUndefined($scope.dropdownChanged) ||
        //     _.isEmpty($scope.dropdownChanged) ||
        //     _.isNull($scope.dropdownChanged)) {
        //     // ============= Optional =============
        //     // Defaults to undefined
        //   }
        //   if (_.isUndefined($scope.dropdownSelectedOption) ||
        //     _.isEmpty($scope.dropdownSelectedOption) ||
        //     _.isNull($scope.dropdownSelectedOption)) {
        //     // ============= Optional =============
        //     // Starts out undefined and then is set internally every time selection changes
        //     // If you set this on the directive that is the model name you will be able to access it externally by
        //   }
        // }
        // TODO: figure out why all those errors are being thrown when calling this function
        // initializeOptions() // Run initialize options the first time the link function runs

        // This is where we set up the magic of tracking dropdown selection changes in both directions
        $scope.__model__ = {
          __tracker__: {}
        }

        // Not sure if this is necessary yet. Saw it as a response in Stack Overflow suggesting it as a fix for some browsers not triggering the change event when making the selection via the keyboard and tabbing away instead of clicking
        // element.bind("keyup", function () {
        //   element.triggerHandler("change")
        // })

        $scope.dropdownChangedInternal = function (dropdownSelectedOption) {
          // When the dropdown changes internal to the directive
          // Call the external callback function
          if (_.isFunction($scope.dropdownChanged)) {
            $scope.dropdownChanged(dropdownSelectedOption)
          }
        }

        function exportSelectedOption () {
          // This function will find the matching object from the dropdownOptions array and make it available outside the directive via dropdownSelectedOption.
          // Very handy if you're trying to show the contents of the selected object without messing up your _id only references for mongo documents

          SaneHydrationService.findMatchingOption(
            _.get($scope, '__model__.__tracker__.' + $scope.dropdownTrackBy),
            $scope.dropdownTrackBy,
            $scope.dropdownOptions
          )
            .then(function (matchingOption) {
              $scope.dropdownSelectedOption = matchingOption
            })

          // Fire the dropdownChanged function, for some reason ng-change is not working
          $scope.dropdownChangedInternal($scope.dropdownSelectedOption)
        }

        function setUpModel (model) {
          // Empty the tracking object again so we don't wind up with weird data bindings or accidentally overwrite the data in the dropdownOptions
          $scope.__model__ = {
            __tracker__: {}
          }

          // Set the tracking key to a copy of the external model selection
          $scope.__model__.__tracker__[$scope.dropdownTrackBy] = angular.copy(model)

          // Every time the model changes, find the matching object and export it outside the directive again
          exportSelectedOption()
        }
        setUpModel($scope.dropdownModel) // Execute on first load

        function exportModel (model) {
          if (_.isUndefined(model) || _.isNull(model) || _.isUndefined(_.get(model, $scope.dropdownTrackBy))) {
            $scope.dropdownModel = null
          } else {
            $scope.dropdownModel = model[$scope.dropdownTrackBy]
          }

          exportSelectedOption()
        }

        // Watch the model external to the directive and bring changes internally
        $scope.$watch('dropdownModel', function (newValue, oldValue) {
          if (newValue !== oldValue) {
            // Keep the internal model in synch by setting it up fresh each time
            setUpModel(newValue)
          }
        })

        // Watch the internally tracked model and send changes out
        $scope.$watch('__model__.__tracker__', function (newValue, oldValue) {
          if (newValue !== oldValue) {
            // TODO: find out why this is firing the external dropdownChanged function twice
            // TODO: considering changing this to a non-deep watch and monitoring the [$scope.dropdownTrackBy] key somehow

            // Export the model outside the directive
            exportModel(newValue)
          }
        }, true)
      }
    }
  }])
})(window, window.angular)
