# angular-sane-dropdown

[![js-standard-style](https://img.shields.io/badge/code%20style-standard-brightgreen.svg)](http://standardjs.com/)

An AngularJS directive for handling dropdowns in a sane way.

# Install
`bower install angular-sane-dropdown`

Add `<script src="bower_components/angular-sane-dropdown/release/angular-sane-dropdown.js"></script>` to your site.

Add `angularSaneDropdown` to your app dependencies.

# Dependencies
This project currently depends on lodash, planned for future updates is to remove this dependency, but for now you'll have to include:

`<script src="bower_components/lodash/lodash.min.js"></script>`

# Usage
These examples are based on usage with MongoDB ObjectID references and angular-sane-dropdown defaults to `_id` as the track by, but that can be overridden with the `dropdown-track-by` property.

Example use of this directive with minimum properties:

```html
<sane-dropdown
  dropdown-model="originalObject._id"
  dropdown-options="dropdownOptions"
  dropdown-display-name="'name'">
</sane-dropdown>
```

Example use of this directive with all properties:

```html
<sane-dropdown
  dropdown-model="originalObject._id"
  dropdown-options="dropdownOptions"
  dropdown-display-name="'name'"
  dropdown-track-by="'_id'"
  dropdown-selected-option="dropdownSelectedOption"
  dropdown-empty-option="'– Select Something –'"
  dropdown-form-name="'test'"
  dropdown-required="false"
  dropdown-disabled="false"
  dropdown-changed="dropdownChanged">
</sane-dropdown>
```

### Properties
`dropdown-model`:
**Required**.
This should only be a string to work properly, not an object.

`dropdown-options`:
**Required**.
Should always be an array.

`dropdown-display-name`:
**Required**.
Must be a string naming the key you want to be the display name for each option. E.g. `dropdown-display-name="'name'"` (note the use of both double and single quotes together).

`dropdown-track-by`:
Optional.
Must be a string naming the key you want to track, E.g. `dropdown-track-by="'_id'"` (note the use of both double and single quotes together).

`dropdown-selected-option`:
Optional.
Feeds out the full object that is chosen in the dropdown from the dropdownOptions array
This will be changed every time the selection is changed in the dropdown; it is the easiest way to get a full copy of the selection outside the directive.

`dropdown-empty-option`:
Optional.
Must be a string. Will be the empty state placeholder option if it is set. E.g. `dropdown-empty-option="'Select something'"` (note the use of both double and single quotes together).

`dropdown-form-name`:
Optional.
Will be the name of this select element for use in forms. E.g. `dropdown-form-name="'test'"` (note the use of both double and single quotes together).

`dropdown-required`:
Optional.
Will set this select element as required for a form.

`dropdown-disabled`:
Optional.
Will disable the dropdown.

`dropdown-changed`:
Optional.
Must be a function name that will be called back when the dropdown selection changes. E.g. `dropdown-changed="'dropdownChangedCallback'"` (note the use of both double and single quotes together).

# Planned Updates
In no particular order:

- Add tests
- Add example page to demonstrate usage
- Remove lodash dependency
